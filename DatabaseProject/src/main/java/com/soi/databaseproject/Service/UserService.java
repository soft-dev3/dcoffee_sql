/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.databaseproject.Service;

import com.soi.databaseproject.dao.UserDao;
import com.soi.databaseproject.model.User;
import java.util.List;

/**
 *
 * @author user
 */
public class UserService {

    public User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
        if (user != null & user.getPassword().equals(password)) {
            return user;
        }
        return null;

    }

    public List<User> getUsers() {
        UserDao userDao = new UserDao();
        return userDao.getAll("user_id asc");
    }
}
