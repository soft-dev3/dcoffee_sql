/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.databaseproject;

import com.soi.databaseproject.Service.UserService;
import com.soi.databaseproject.model.User;

/**
 *
 * @author user
 */
public class TestUserService {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("soisawan", "ssw1234");
        if (user != null) {
            System.out.println("WELCOME USER : " + user.getName());
        } else {
            System.out.println("ERROR");
        }
    }

}
