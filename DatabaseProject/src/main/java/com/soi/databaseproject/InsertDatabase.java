package com.soi.databaseproject;

import com.soi.databaseproject.helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertDatabase {

    public static void main(String[] args) {

        Connection conn = null;
        conn = DatabaseHelper.getConnect();
        String sql = "INSERT INTO category(category_id, category_name) VALUES (?,?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 3);
            stmt.setString(2, "candy");
            int status = stmt.executeUpdate();
//            ResultSet key = stmt.getGeneratedKeys();
//            key.next();
//            System.out.println(" " + key.getInt(1));

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        DatabaseHelper.close();
    }

}
