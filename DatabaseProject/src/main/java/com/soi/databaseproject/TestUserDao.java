/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.databaseproject;

import com.soi.databaseproject.dao.UserDao;
import com.soi.databaseproject.helper.DatabaseHelper;
import com.soi.databaseproject.model.User;

/**
 *
 * @author user
 */
public class TestUserDao {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        System.out.println(userDao.getAll());
//        System.out.println(userDao.get(1));
//
//        User user = new User("UserA", "M", 1, "1234");
////        userDao.save(user);
//        System.out.println(userDao.getAll());
//
//        User user3 = userDao.get(3);
//        user3.setGender("F");
//        userDao.update(user3);
//        User updateUser = userDao.get(user3.getId());
//        System.out.println("update: " + updateUser);

//        userDao.delete(user3);
//        for (User u : userDao.getAll()) {
//            System.out.println(u);
//        }
        for (User u : userDao.getAll("user_name  like 'u%' ", "user_name asc, user_gender desc")) {
            System.out.println(u);
        }
        DatabaseHelper.close();
    }

}
