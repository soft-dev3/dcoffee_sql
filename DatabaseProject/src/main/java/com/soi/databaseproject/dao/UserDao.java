/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.databaseproject.dao;

import com.soi.databaseproject.helper.DatabaseHelper;
import com.soi.databaseproject.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class UserDao implements Dao<User> {

    @Override
    public User get(int id) {
        User user = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM user WHERE user_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                user = User.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }

    @Override
    public List<User> getAll() {
        ArrayList<User> list = new ArrayList<>();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM user";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public User getByLogin(String name) {
        User user = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM user WHERE user_name=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                user = User.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }

    public List<User> getAll(String where, String order) {
        ArrayList<User> list = new ArrayList<>();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM user where " + where + "  ORDER BY " + order;
        System.out.println(sql);
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<User> getAll(String order) {
        ArrayList<User> list = new ArrayList<>();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM user ORDER BY " + order;
        System.out.println(sql);
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public User save(User obj) {

        Connection conn = DatabaseHelper.getConnect();
        String sql = "INSERT INTO user (user_name, user_gender, user_role, user_password ) VALUES (?, ?, ?, ?, ?) ";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getLogin());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getGender());
            stmt.setInt(4, obj.getRole());
            stmt.setString(5, obj.getPassword());

//            System.out.println(stmt);
            stmt.executeUpdate();
            obj.setId(DatabaseHelper.getInsertedId(stmt));

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return obj;
    }

    @Override
    public User update(User obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "UPDATE user SET user_name = ?,user_gender = ?,user_password = ?,user_role = ? WHERE user_id = ? ";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getLogin());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getGender());
            stmt.setInt(4, obj.getRole());
            stmt.setString(5, obj.getPassword());
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int res = stmt.executeUpdate();
            System.out.println("res: " + res);
            return obj;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public int delete(User obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "DELETE FROM user WHERE user_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
